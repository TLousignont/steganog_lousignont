
import java.awt.Color;
import java.io.IOException;
import java.nio.ByteBuffer;

import edu.neumont.ui.Picture;

public class Steganog {
	private PrimeIterator primI;
	/**
	* Takes a clean image and changes the prime-indexed pixels to secretly carry the message
	**/
	public Picture embedIntoImage(Picture cleanImage, String message) throws IOException {
		int numPixels = cleanImage.height() * cleanImage.width();
		primI = new PrimeIterator(numPixels);
		char[] messageChar = message.toUpperCase().toCharArray();
		int PrimN = 2;
		for(int i = 0; i < messageChar.length; i ++)
		{
			if(primI.hasNext())
			{
				//Determine what row of the image we are on
				int row = PrimN/cleanImage.width();
				int column = (PrimN % cleanImage.width()) -1; // 0 based
				//Get the Color value of that pixel
				Color color = cleanImage.get(row, column);
				String red = Integer.toBinaryString(color.getRed());
				String green = Integer.toBinaryString(color.getGreen());
				String blue = Integer.toBinaryString(color.getBlue());
				int character = (int)messageChar[i] - 32;
				String bitsSub1 = Integer.toBinaryString(getXthBit(character, 5)) + Integer.toBinaryString(getXthBit(character, 4));
				String redBin = setlastBits(red, bitsSub1);
				String bitsSub2 = Integer.toBinaryString(getXthBit(character, 3)) + Integer.toBinaryString(getXthBit(character, 2));
				String greenBin = setlastBits(green, bitsSub2);
				String bitsSub3 = Integer.toBinaryString(getXthBit(character, 1)) + Integer.toBinaryString(getXthBit(character, 0));
				String blueBin = setlastBits(blue, bitsSub3);
			
				
				//produce color with hidden character. 
				int r = Integer.parseInt(redBin, 2);
				int g = Integer.parseInt(greenBin, 2); 
				int b = Integer.parseInt(blueBin,2);
				color = new Color(r,g,b);
				cleanImage.set(row, column, color);
				PrimN = primI.next();
			}
		}
		return cleanImage;
	}
	
	/**
	* Retreives the embedded secret from the secret-carrying image
	*/
	public String retreiveFromImage(Picture imageWithSecretMessage) throws IOException {
		String message = new String();
		int numPixels = imageWithSecretMessage.height() * imageWithSecretMessage.width();
		primI = new PrimeIterator(numPixels);
		int PrimN = 2;
		while(primI.hasNext())
		{
			//Determine what row and column of the image we are on
			int row = PrimN/imageWithSecretMessage.width();
			int column = (PrimN % imageWithSecretMessage.width()) - 1; // 0 based
			//Get the Color value of that pixel
			Color color = imageWithSecretMessage.get(row, column);
			int red = color.getRed();
			int green = color.getGreen();
			int blue = color.getBlue();
			String characterBin = "";
			characterBin += getXthBit(red, 1);//0
			characterBin += getXthBit(red, 0);//1
			characterBin += getXthBit(green, 1);//2
			characterBin += getXthBit(green, 0);//3
			characterBin += getXthBit(blue, 1);//4
			characterBin += getXthBit(blue, 0);//5
			int charValue = Integer.parseInt(characterBin,2);
			char cha = (char)(charValue + 32);
			message += cha;
			//Get the next prime number: 2, 3, 5....
			PrimN = primI.next();
		}
		return message; 
	}
	
	private int getXthBit(int n, int x) {
	    return (n >> x) & 1;
	}
	
	private String setlastBits(String number, String lastTwoBits){
		String result = number;
		if(number.length() > 2) {
			String sub = number.substring(0, number.length() - 2);
			result = sub + lastTwoBits;
		}
		else {
			result = lastTwoBits;
		}
		return result;
	}
	
	private int setBitAt(int targetInt, int bitChanged, int value) {
		int returnInt = targetInt;
		int bitAtValue = getXthBit(targetInt, targetInt);
		if(bitAtValue == value)
		{
			returnInt = targetInt;
		}
		else 
		{
			if(bitAtValue == 0){
				returnInt = targetInt | (1 << bitChanged);
			}
			else {
				returnInt = targetInt & ~(1 << bitChanged);
			}
			
		}
		return returnInt;
	}
	

}
