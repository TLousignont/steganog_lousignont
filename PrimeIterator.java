

import java.util.Iterator;
import java.math.*;
public class PrimeIterator implements Iterator<Integer>{

	int maximumNumber = 0;
	boolean[] numbers; 
	
	int startingIndex = 2;
	int currentNum = 2;
	
	public PrimeIterator(int max) {
		maximumNumber = max + 1;
		numbers = new boolean[maximumNumber];
		numbers[0] = false;
		numbers[1] = false;
		for(int i = 2; i < maximumNumber; i++)
		{
			numbers[i] = true;
		}
		genPrimes();
	}
	
	private void genPrimes(){
		for(int i = startingIndex; i <maximumNumber; i ++) {
			if(numbers[i]) {
				int multi = 0;
				for(int j = (i)*(i); j < maximumNumber; j += (i)) {
					numbers[j] = false;
					multi ++;
				}
			}
		}
	}
	
	
	@Override
	public boolean hasNext() {
		for(int i = startingIndex; i < maximumNumber; i++ ){
			if(numbers[i] && i > currentNum)
			{
				currentNum = i;
				return true;
			}
		}
		return false;
	}

	@Override
	public Integer next() {
		return currentNum;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

}
